import matplotlib.gridspec as gridspec
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import tree
from sklearn.model_selection import train_test_split
import _pickle as cPickle
import numpy as np
import pandas as pd
from sklearn.datasets import make_moons
from sklearn.metrics import f1_score
from sklearn import svm
import copy

import rule_extractor
import diro2c
from data import preprocess_data
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.global_data_generation import global_data_generator
from data_generation.helper import *
import evaluation

from enums.dataset_type import dataset_type
from enums.diff_classifier_method_type import diff_classifier_method_type


def main():

    # X, y = make_classification(n_features=2, n_redundant=0, n_informative=1,
    #                            n_clusters_per_class=1)

    # Create classification sample:
    # X1, y1 = make_classification(n_samples=300, n_features=2, n_redundant=0, n_informative=2,
    #                              n_clusters_per_class=1, class_sep=0.75, random_state=0, scale=100)

    X1, y1 = make_classification(n_samples=300, n_features=2, n_redundant=0, n_informative=2,
                                 n_clusters_per_class=1, class_sep=1.3, random_state=0, flip_y=0, scale=100)

    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    y = y1.astype(str)

    d = {'x1': feature1, 'x2': feature2, 'y': y}

    df1 = pd.DataFrame(d)

    df1.to_csv('diro2c_diagonal_ex_dataset.csv', index=False)

    X1_train, X1_test, y1_train, y1_test = train_test_split(
        X1, y1, test_size=0.2, random_state=1)

    print('df1:')
    print('y1: ', np.unique(y1, return_counts=True))
    print('y1_train: ', np.unique(y1_train, return_counts=True))
    print('feature1 - min: ', np.min(df1['x1']))
    print('feature1 - max: ', np.max(df1['x1']))
    print('feature1 - mean: ', np.mean(df1['x1']))
    print('feature1 - std ', np.std(df1['x1']))

    print('feature2 - min: ', np.min(df1['x2']))
    print('feature2 - max: ', np.max(df1['x2']))
    print('feature2 - mean: ', np.mean(df1['x2']))
    print('feature2 - std ', np.std(df1['x2']))

    # -----------------------------------------------
    # train BB 1:

    blackbox1 = LogisticRegression(random_state=1)
    blackbox1.fit(X1_train, y1_train)

    # -----------------------------------------------
    # manipulate and train BB 2:
    X2 = copy.deepcopy(X1)
    y2 = copy.deepcopy(y1)
    y2_new = []
    for x_0, y_0 in zip(X2, y2):
        if x_0[0] < 100:
            y2_new.append(0)
        else:
            y2_new.append(1)

    X2 = np.asarray(X2)
    y2 = np.asarray(y2_new)

    feature1 = []
    feature2 = []
    for x in X2:
        feature1.append(x[0])
        feature2.append(x[1])

    y = y2.astype(str)

    d = {'x1': feature1, 'x2': feature2, 'y': y}

    df2 = pd.DataFrame(d)

    df2.to_csv('diro2c_diagonal_ex_manipulated_dataset.csv', index=False)

    X2_train, X2_test, y2_train, y2_test = train_test_split(
        X2, y2, test_size=0.2, random_state=1)

    print('df2:')
    print('y2: ', np.unique(y2, return_counts=True))
    print('y2_train: ', np.unique(y2_train, return_counts=True))
    print('feature1 - min: ', np.min(df2['x1']))
    print('feature1 - max: ', np.max(df2['x1']))
    print('feature1 - mean: ', np.mean(df2['x1']))
    print('feature1 - std ', np.std(df2['x1']))

    print('feature2 - min: ', np.min(df2['x2']))
    print('feature2 - max: ', np.max(df2['x2']))
    print('feature2 - mean: ', np.mean(df2['x2']))
    print('feature2 - std ', np.std(df2['x2']))

    blackbox2 = LogisticRegression(random_state=1)
    blackbox2.fit(X2_train, y2_train)
    # -------------------------------------------------

    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    for x in X2:
        feature1.append(x[0])
        feature2.append(x[1])

    feature1 = np.asarray(feature1)
    feature2 = np.asarray(feature2)

    y = np.concatenate((y1, y2))

    y = y.astype(str)

    d = {'y': y, 'feature1': feature1, 'feature2': feature2}

    dfc = pd.DataFrame(d)

    dataset = prepare_df(dfc, 'test', 'y', reorder=False)

    dfc = dataset['df']

    X_to_recognize_diff = np.concatenate((X1_test, X2_test))

    # xs = 0
    # for xx in X_to_recognize_diff:
    #     xs += 1
    #     y = blackbox1.decision_function(np.array(xx).reshape(1, -1))
    #     w_norm = np.linalg.norm(blackbox1.coef_)
    #     dist = y / w_norm
    #     print('Distance of xs: ', xs, ', Distance: ', dist)
    #     print(xx)

    # min_idx = None
    # min_x = None
    # i = 0
    # for x in X_to_recognize_diff:
    #     if min_x is None:
    #         min_x = abs(x[0]) + abs(x[1])
    #         min_idx = i
    #     else:
    #         if (abs(x[0]) + abs(x[1])) < min_x:
    #             min_x = abs(x[0]) + abs(x[1])
    #             min_idx = i

    #     i += 1

    # instances_2_explain = [2, 3, 5, 8, 9, 26, 36, 62, 63, 68, 69, 85, 86]
    instances_2_explain = list()
    i = 1
    j = 1
    index = 1

    print("-------------------------------------")

    for x in X_to_recognize_diff:

        y = blackbox1.decision_function(np.array(x).reshape(1, -1))
        w_norm = np.linalg.norm(blackbox1.coef_)
        dist = y / w_norm

        bb1predict = blackbox1.predict(np.array(x).reshape(1, -1))
        bb2predict = blackbox2.predict(np.array(x).reshape(1, -1))
        if bb1predict != bb2predict and i <= 10 and dist < 20:
            instances_2_explain.append(index)
            i += 1

        if bb1predict == bb2predict and j <= 10 and dist < 20:
            instances_2_explain.append(index)
            j += 1

        index += 1

        if i == 10 and j == 10:
            print("enough")
            break

    print("----------------------------------")

    X_diff = None
    y_diff = None

    for i in instances_2_explain:

        print(i)
        print(X_to_recognize_diff[i])

        diff_classifiers_info = diro2c.recognize_diff(i, X_to_recognize_diff, dataset, blackbox1, blackbox2,
                                                      diff_classifier_method_type.multiclass_diff_classifier,
                                                      data_generation_function=neighbor_generator.get_modified_genetic_neighborhood)
        # data_generation_function=global_data_generator.get_global_synthetic_random_dataset)

        # %matplotlib inline
        # gs = gridspec.GridSpec(3, 2)
        # fig = plt.figure(figsize=(14, 10))
        # labels = ['Logistic Regression', 'Decision Tree',
        #          'Random Forest', 'SVM', 'Naive Bayes', 'Neural Network']

        dc_info = diff_classifiers_info['multiclass_diff_classifer']
        evaluation_info = dc_info['evaluation_info']

        if X_diff is None and y_diff is None:
            X_diff = evaluation_info['X']
            y_diff = evaluation_info['y']
        else:
            X_diff = np.concatenate((X_diff, evaluation_info['X']))
            y_diff = np.concatenate((y_diff, evaluation_info['y']))

    dc_full = DecisionTreeClassifier(random_state=0)
    dc_full.fit(X_diff, y_diff)

    # my_cmap = plt.cm.get_cmap("jet").copy()
    # my_cmap.set_under('w', 1)

    # plt.hist2d([item[0] for item in X_diff], [item[1]
    #                                           for item in X_diff], bins=(60, 60), cmap=my_cmap, vmin=1, vmax=100)
    # cb = plt.colorbar()
    # cb.set_label('counts in bin')
    # ax = plt.gca()
    # ax.set_xlim([-120, 350])
    # ax.set_ylim([-400, 270])
    # ax.set_xlabel('x1')
    # ax.set_ylabel('x2')
    # plt.title("data density of dataset - modified genetic neighborhood")
    # plt.show()

    # print(f1_score(y_test_true, y_test_dc))

    print(np.unique(y_diff, return_counts=True))

    # fig, ax = plt.subplots(1, 2, figsize=(16, 8))

    # fig = plot_decision_regions(X=X2_train.astype(np.int64), y=y2_train.astype(
    #     np.int64), clf=blackbox2, ax=ax[0], legend=2,
    #     set_background=True, hide_spines=False, set_decision_boundary_line=True)
    # ax[0].set_xlabel('x1')
    # ax[0].set_ylabel('x2')
    # ax[0].set_title('trained black box A')
    # ax[0].set_xlim([-120, 350])
    # ax[0].set_ylim([-400, 270])

    # fig = plot_decision_regions(X=X1_train.astype(np.int64), y=y1_train.astype(
    #     np.int64), clf=blackbox1, ax=ax[1], legend=2,
    #     set_background=True, hide_spines=False, set_decision_boundary_line=True)
    # ax[1].set_xlabel('x1')
    # ax[1].set_ylabel('x2')
    # ax[1].set_title('trained black box B')
    # ax[1].set_xlim([-120, 350])
    # ax[1].set_ylim([-400, 270])

    # plt.show()

    # fig = plot_decision_regions(X=X_diff, y=y_diff.astype(
    #     np.int64), clf=dc_full, ax=ax[2], legend=2,
    #     set_background=False, hide_spines=False, set_decision_boundary_line=True)
    # ax[2].set_xlabel('feature 1')
    # ax[2].set_ylabel('feature 2')
    # ax[2].set_title('modified genetic neighborhood - binary diff-classifier')
    # ax[2].set_xlim([-200, 300])
    # ax[2].set_ylim([20, 200])

    # handles, labels = ax[2].get_legend_handles_labels()

    # plt.legend(handles,
    #            ['no diff', 'diff'],
    #            framealpha=0.3, scatterpoints=1)

    # plot_decision_regions(X=X_diff.astype(np.int64), y=y_diff.astype(
    #     np.int64), clf=dc_full, legend=2)

    ax = plot_decision_regions(X=X_diff, y=y_diff, clf=dc_full, legend=2,
                               set_background=True, hide_spines=False, set_decision_boundary_line=True)

    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.title('modified genetic neighborhood - multiclass diff-classifier')
    plt.xlim([-120, 350])
    plt.ylim([-400, 270])

    handles, labels = ax.get_legend_handles_labels()

    # plt.legend(handles,
    #            ['no diff', 'diff'],
    #            framealpha=0.3, scatterpoints=1)
    plt.legend(handles,
               ['class 00', 'class 11', 'class 10', 'class 01'],
               framealpha=0.3, scatterpoints=1)

    plt.show()
    # # plt.savefig('images/decision_boundaries.png')

    fn = ['x1', 'x2']
    #cn = ['no diff', 'diff']
    cn = ['00', '11', '10', '01']
    fig3 = plt.figure(dpi=600)
    tree.plot_tree(dc_full,
                   feature_names=fn,
                   class_names=cn,
                   filled=True)
    fig3.savefig('images/decision_tree.png')

    rule_extractor.print_rules_for_binary(
        dc_full, dataset['columns_for_decision_rules'], ['no diff', 'diff'], 'diff')


if __name__ == "__main__":
    main()
