import matplotlib.gridspec as gridspec
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree
from sklearn.model_selection import train_test_split
import _pickle as cPickle
import numpy as np
import pandas as pd
from sklearn.datasets import make_moons
from sklearn.metrics import f1_score
from sklearn import svm
import copy

import rule_extractor
import diro2c
from data import preprocess_data
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.global_data_generation import global_data_generator
from data_generation.helper import *
import evaluation

from enums.dataset_type import dataset_type
from enums.diff_classifier_method_type import diff_classifier_method_type


def main():

    # X, y = make_classification(n_features=2, n_redundant=0, n_informative=1,
    #                            n_clusters_per_class=1)

    # Create classification sample:
    X1, y1 = make_classification(n_samples=300, n_features=2,
                                 n_informative=1, n_redundant=0, n_classes=2, random_state=2, n_clusters_per_class=1, class_sep=1.8, flip_y=0, scale=100)

    X1_train, X1_test, y1_train, y1_test = train_test_split(
        X1, y1, test_size=0.2, random_state=1)
    # -----------------------------------------------
    # train BB 1:

    blackbox1 = DecisionTreeClassifier(random_state=1)
    blackbox1.fit(X1_train, y1_train)

    # -----------------------------------------------
    # manipulate and train BB 2:
    X2 = copy.deepcopy(X1)
    y2 = copy.deepcopy(y1)
    y2_new = []
    for x_0, y_0 in zip(X2, y2):
        if x_0[0] < 150 and x_0[0] > 0 and x_0[1] > -100 and x_0[1] < 100 and y_0 == 1:
            y2_new.append(0)
        elif x_0[0] < 0 and x_0[0] > -200 and x_0[1] >= 100 and y_0 == 0:
            y2_new.append(1)
        else:
            y2_new.append(y_0)

    X2 = np.asarray(X2)
    y2 = np.asarray(y2_new)

    X2_train, X2_test, y2_train, y2_test = train_test_split(
        X2, y2, test_size=0.2, random_state=1)

    blackbox2 = DecisionTreeClassifier(random_state=1)
    blackbox2.fit(X2_train, y2_train)
    # -------------------------------------------------

    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    for x in X2:
        feature1.append(x[0])
        feature2.append(x[1])

    feature1 = np.asarray(feature1)
    feature2 = np.asarray(feature2)

    y = np.concatenate((y1, y2))

    y = y.astype(str)

    d = {'y': y, 'feature_1': feature1, 'feature_2': feature2}

    df = pd.DataFrame(d)

    dataset = prepare_df(df, 'test', 'y')

    df2 = dataset['df']

    print('feature1 - min: ', np.min(df2['feature_1']))
    print('feature1 - max: ', np.max(df2['feature_1']))
    print('feature1 - mean: ', np.mean(df2['feature_1']))
    print('feature1 - std ', np.std(df2['feature_1']))

    print('feature2 - min: ', np.min(df2['feature_2']))
    print('feature2 - max: ', np.max(df2['feature_2']))
    print('feature2 - mean: ', np.mean(df2['feature_2']))
    print('feature2 - std ', np.std(df2['feature_2']))

    X_to_recognize_diff = np.concatenate((X1_test, X2_test))

    # idx: 100 --> 1st seed

    print(X_to_recognize_diff[100])

    diff_classifiers_info = diro2c.recognize_diff(100, X_to_recognize_diff, dataset, blackbox1, blackbox2,
                                                  diff_classifier_method_type.multiclass_diff_classifier,
                                                  data_generation_function=neighbor_generator.get_closed_real_data)
    # data_generation_function=global_data_generator.get_global_real_data)

    # %matplotlib inline
    # gs = gridspec.GridSpec(3, 2)
    # fig = plt.figure(figsize=(14, 10))s
    # labels = ['Logistic Regression', 'Decision Tree',
    #          'Random Forest', 'SVM', 'Naive Bayes', 'Neural Network']

    dc_info = diff_classifiers_info['multiclass_diff_classifer']
    dc_full = dc_info['dc_full']
    dc_test = dc_info['dc_test']
    evaluation_info = dc_info['evaluation_info']

    X_diff = evaluation_info['X']
    y_diff = evaluation_info['y']

    y_test_true = evaluation_info['y_test_true']
    y_test_dc = evaluation_info['y_test_dc']

    my_cmap = plt.cm.jet
    my_cmap.set_under('w', 1)

    plt.hist2d([item[0] for item in X_diff], [item[1]
                                              for item in X_diff], bins=(80, 60), cmap=my_cmap, vmin=1, vmax=50)
    cb = plt.colorbar()
    cb.set_label('counts in bin')
    ax = plt.gca()
    ax.set_xlim([-300, 400])
    ax.set_ylim([-260, 320])
    ax.set_xlabel('x1')
    ax.set_ylabel('x2')
    plt.title("data density of dataset")
    plt.show()

    # print(f1_score(y_test_true, y_test_dc))

    print(np.unique(y_diff, return_counts=True))

    fig, ax = plt.subplots(1, 2, figsize=(16, 8))

    fig = plot_decision_regions(X=X1.astype(np.int64), y=y1.astype(
        np.int64), clf=blackbox1, ax=ax[0], legend=2,
        set_background=True, hide_spines=False, set_decision_boundary_line=True)
    ax[0].set_xlabel('x1')
    ax[0].set_ylabel('x2')
    ax[0].set_title('trained black box A')
    ax[0].set_xlim([-300, 400])
    ax[0].set_ylim([-260, 320])

    fig = plot_decision_regions(X=X2.astype(np.int64), y=y2.astype(
        np.int64), clf=blackbox2, ax=ax[1], legend=2,
        set_background=True, hide_spines=False, set_decision_boundary_line=True)
    ax[1].set_xlabel('x1')
    ax[1].set_ylabel('x2')
    ax[1].set_title('trained black box B')
    ax[1].set_xlim([-300, 400])
    ax[1].set_ylim([-260, 320])

    plt.show()

    # fig = plot_decision_regions(X=X_diff, y=y_diff.astype(
    #     np.int64), clf=dc_full, ax=ax[2], legend=2,
    #     set_background=False, hide_spines=False, set_decision_boundary_line=True)
    # ax[2].set_xlabel('feature 1')
    # ax[2].set_ylabel('feature 2')
    # ax[2].set_title('modified genetic neighborhood - binary diff-classifier')
    # ax[2].set_xlim([-200, 300])
    # ax[2].set_ylim([20, 200])

    # handles, labels = ax[2].get_legend_handles_labels()

    # plt.legend(handles,
    #            ['no diff', 'diff'],
    #            framealpha=0.3, scatterpoints=1)

    # plot_decision_regions(X=X_diff.astype(np.int64), y=y_diff.astype(
    #     np.int64), clf=dc_full, legend=2)

    ax = plot_decision_regions(X=X_diff, y=y_diff, clf=dc_full, legend=2,
                               set_background=False, hide_spines=False, set_decision_boundary_line=False)

    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.title('genetic neighborhood - multiclass diff-classifier')
    plt.xlim([-300, 400])
    plt.ylim([-300, 320])

    handles, labels = ax.get_legend_handles_labels()

    # plt.legend(handles,
    #            ['no diff', 'diff'],
    #            framealpha=0.3, scatterpoints=1)
    plt.legend(handles,
               ['class 00', 'class 11', 'class 10', 'class 01'],
               framealpha=0.3, scatterpoints=1)

    plt.show()
    # plt.savefig('images/decision_boundaries.png')

    # fn = ['x - feature 1', 'y - feature 2']
    # cn = ['no_diff', 'diff']
    # fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(4, 4), dpi=300)
    # tree.plot_tree(dc_full,
    #                feature_names=fn,
    #                class_names=cn,
    #                filled=True)
    # fig.savefig('images/decision_tree.png')

    fn = ['x1', 'x2']
    cn = ['0', '1']
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 8), dpi=300)
    tree.plot_tree(blackbox1,
                   feature_names=fn,
                   class_names=cn,
                   filled=True)
    fig.savefig('images/decision_tree_bb1.png')

    fn = ['x1', 'x2']
    cn = ['0', '1']
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 8), dpi=300)
    tree.plot_tree(blackbox2,
                   feature_names=fn,
                   class_names=cn,
                   filled=True)
    fig.savefig('images/decision_tree_bb2.png')

    # rule_extractor.print_rules_for_binary(
    #     dc_full, dataset['columns_for_decision_rules'], ['no_diff', 'diff'], 'diff')


if __name__ == "__main__":
    main()
