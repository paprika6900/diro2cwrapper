import matplotlib.gridspec as gridspec
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt
from sklearn.datasets import make_gaussian_quantiles
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import _pickle as cPickle
import numpy as np
import pandas as pd
from sklearn.datasets import make_moons
from sklearn import svm
import copy

import rule_extractor
import diro2c
from data import preprocess_data
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.global_data_generation import global_data_generator
from data_generation.helper import *
import evaluation

from enums.dataset_type import dataset_type
from enums.diff_classifier_method_type import diff_classifier_method_type


def main():

    X1, y1 = make_gaussian_quantiles(n_samples=300,
                                     n_classes=2, shuffle=False, cov=0.8, random_state=7)
    X1 = X1*100
    # X1 = np.append(X1, [[1.1, 1.1]], axis=0)
    # y1 = np.append(y1, 0)

    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    y = y1.astype(str)

    d = {'x1': feature1, 'x2': feature2, 'y': y}

    df1 = pd.DataFrame(d)

    print('df1:')
    print('feature1 - min: ', np.min(df1['x1']))
    print('feature1 - max: ', np.max(df1['x1']))
    print('feature1 - mean: ', np.mean(df1['x1']))
    print('feature1 - std ', np.std(df1['x1']))

    print('feature2 - min: ', np.min(df1['x2']))
    print('feature2 - max: ', np.max(df1['x2']))
    print('feature2 - mean: ', np.mean(df1['x2']))
    print('feature2 - std ', np.std(df1['x2']))

    df1.to_csv('diro2c_gaussian_dataset.csv', index=False)

    X1_train, X1_test, y1_train, y1_test = train_test_split(
        X1, y1, test_size=0.2, random_state=1)

    # blackbox1 = DecisionTreeClassifier(random_state=1)
    blackbox1 = svm.SVC(random_state=1)
    blackbox1.fit(X1_train, y1_train)

    X2, y2 = make_gaussian_quantiles(n_samples=300,
                                     n_classes=2, shuffle=False, cov=1.3, random_state=7)
    X2 = X2*100
    # X2 = np.append(X2, [[1.1, 1.1]], axis=0)
    # y2 = np.append(y2, 0)

    feature1 = []
    feature2 = []
    for x in X2:
        feature1.append(x[0])
        feature2.append(x[1])

    y = y2.astype(str)

    d = {'x1': feature1, 'x2': feature2, 'y': y}

    df2 = pd.DataFrame(d)

    print('df2:')
    print('feature1 - min: ', np.min(df2['x1']))
    print('feature1 - max: ', np.max(df2['x1']))
    print('feature1 - mean: ', np.mean(df2['x1']))
    print('feature1 - std ', np.std(df2['x1']))

    print('feature2 - min: ', np.min(df2['x2']))
    print('feature2 - max: ', np.max(df2['x2']))
    print('feature2 - mean: ', np.mean(df2['x2']))
    print('feature2 - std ', np.std(df2['x2']))

    df2.to_csv('diro2c_gaussian_manipulated_dataset.csv', index=False)

    X2_train, X2_test, y2_train, y2_test = train_test_split(
        X2, y2, test_size=0.2, random_state=1)

    #blackbox2 = DecisionTreeClassifier(random_state=1)
    blackbox2 = svm.SVC(random_state=1)
    blackbox2.fit(X2_train, y2_train)
    # -------------------------------------------------

    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    for x in X2:
        feature1.append(x[0])
        feature2.append(x[1])

    feature1 = np.asarray(feature1)
    feature2 = np.asarray(feature2)

    y = np.concatenate((y1, y2))

    y = y.astype(str)

    d = {'y': y, 'feature_1': feature1, 'feature_2': feature2}

    df = pd.DataFrame(d)

    dataset = prepare_df(df, 'test', 'y')

    min_idx = None
    min_x = None
    i = 0
    for x in dataset['X']:
        if min_x is None:
            min_x = abs(x[0]) + abs(x[1])
            min_idx = i
        else:
            if (abs(x[0]) + abs(x[1])) < min_x:
                min_x = abs(x[0]) + abs(x[1])
                min_idx = i

        i += 1

    # seed 2 = 147
    tbd = min_idx
    print(dataset['X'][tbd])

    diff_classifiers_info = diro2c.recognize_diff(tbd, dataset['X'], dataset, blackbox1, blackbox2,
                                                  diff_classifier_method_type.binary_diff_classifier,
                                                  data_generation_function=neighbor_generator.get_modified_genetic_neighborhood)

    # %matplotlib inline
    # gs = gridspec.GridSpec(3, 2)
    # fig = plt.figure(figsize=(14, 10))
    # labels = ['Logistic Regression', 'Decision Tree',
    #          'Random Forest', 'SVM', 'Naive Bayes', 'Neural Network']

    dc_info = diff_classifiers_info['binary_diff_classifer']
    dc_full = dc_info['dc_full']
    dc_test = dc_info['dc_test']
    evaluation_info = dc_info['evaluation_info']

    X_diff = evaluation_info['X']
    y_diff = evaluation_info['y']

    print(np.unique(y_diff, return_counts=True))

    my_cmap = plt.cm.get_cmap("jet").copy()
    my_cmap.set_under('w', 1)

    plt.hist2d([item[0] for item in X_diff], [item[1]
                                              for item in X_diff], bins=(80, 60), cmap=my_cmap, vmin=1, vmax=100)
    cb = plt.colorbar()
    cb.set_label('counts in bin')
    ax = plt.gca()
    ax.set_xlim([-250, 250])
    ax.set_ylim([-250, 250])
    ax.set_xlabel('x1')
    ax.set_ylabel('x2')
    plt.title("data density of dataset - modified genetic neighborhood")
    plt.show()

    fig, ax = plt.subplots(1, 2, figsize=(16, 8))

    fig = plot_decision_regions(X=X1_train, y=y1_train.astype(
        np.int64), clf=blackbox1, ax=ax[0], legend=2,
        set_background=True, hide_spines=False, set_decision_boundary_line=True)
    ax[0].set_xlabel('x1')
    ax[0].set_ylabel('x2')
    ax[0].set_xlim([-250, 250])
    ax[0].set_ylim([-250, 250])
    ax[0].set_title('trained black box A')

    fig = plot_decision_regions(X=X2_train, y=y2_train.astype(
        np.int64), clf=blackbox2, ax=ax[1], legend=2,
        set_background=True, hide_spines=False, set_decision_boundary_line=True)
    ax[1].set_xlabel('x1')
    ax[1].set_ylabel('x2')
    ax[1].set_xlim([-250, 250])
    ax[1].set_ylim([-250, 250])
    ax[1].set_title('trained black box B')

    # fig = plot_decision_regions(X=X_diff, y=y_diff.astype(
    #     np.integer), clf=dc_full, ax=ax[2], legend=2)
    # ax[2].set_xlabel('feature 1')
    # ax[2].set_ylabel('feature 2')
    # ax[2].set_xlim([-220, 220])
    # ax[2].set_ylim([-220, 220])
    # ax[2].set_title('binary diff-classifier')
    # handles, labels = ax.get_legend_handles_labels()
    # # ax.legend(handles,
    # #           ['class a', 'class b', 'class c', 'class d'],
    # #           framealpha=0.3, scatterpoints=1)

    plt.show()

    ax = plot_decision_regions(X=X_diff, y=y_diff, clf=dc_full, legend=2,
                               set_background=False, hide_spines=False, set_decision_boundary_line=False)

    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.title('modified genetic neighborhood - binary diff-classifier')
    plt.xlim([-250, 250])
    plt.ylim([-250, 250])

    handles, labels = ax.get_legend_handles_labels()

    plt.legend(handles,
               ['no diff', 'diff'],
               framealpha=0.3, scatterpoints=1)

    # plt.legend(handles,
    #            ['class 00', 'class 11', 'class 10', 'class 01'],
    #            framealpha=0.3, scatterpoints=1)

    plt.show()
    # plt.savefig('images/decision_boundaries.png')

    fn = ['x - feature 1', 'y - feature 2']
    cn = ['no_diff', 'diff']
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(4, 4), dpi=300)
    tree.plot_tree(dc_full,
                   feature_names=fn,
                   class_names=cn,
                   filled=True)
    fig.savefig('images/decision_tree.png')

    # rule_extractor.print_rules_for_binary(
    #     dc_full, dataset['columns_for_decision_rules'], ['no_diff', 'diff'], 'diff')


if __name__ == "__main__":
    main()
