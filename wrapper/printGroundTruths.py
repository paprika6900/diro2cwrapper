from random import randrange

import matplotlib.pyplot as plt
from mlxtend.plotting import plot_decision_regions
from sklearn.tree import DecisionTreeClassifier

import diro2c
from blackbox import *
from data_generation.helper import *
from data_generation.neighborhood_generation import neighbor_generator
from enums.diff_classifier_method_type import diff_classifier_method_type
from location_generation import location_generation
from synth_data_generation import synth_data_generation
from wrapper.blackbox import blackbox_falling, blackbox_sinus_falling, blackbox_sinus_broad, blackbox_sinus_reducing, \
    blackbox_isolated, blackbox_lt_0
from wrapper.clusteringType import ClusteringType
from wrapper.diff_blackbox import diff_blackbox
from wrapper.location_generation.location_generation import plot_blackbox, plot_diff_classifier
import sys

def printGroundTruth(diffBlackbox,X1,y1,ax=None):

    bottomLeft = (0, -40)
    topRight = (50, 40)
    xlim = [bottomLeft[0], topRight[0]]
    ylim = [bottomLeft[1], topRight[1]]

    location = X1
    value = y1

    # define blackboxes to use
    blackbox1 = diffBlackbox

    X1 = [bottomLeft, topRight]
    y1 = [0, 1]
    i = 0
    for x_0, y_0 in zip(location, value):
        X1.append(x_0)
        y1.append(blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0])
    X1 = np.asarray(X1)
    y1 = np.asarray(y1)
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5, 8), dpi=80)
        plot_diff_classifier(ax, X1, y1, blackbox1, xlim, ylim);
        ax.set_title(blackbox1.name)

        plt.savefig(blackbox1.name + '_groundTruth.png', bbox_inches='tight')
    else:
        plot_diff_classifier(ax, X1, y1, blackbox1, xlim, ylim);
        ax.set_title(blackbox1.name)

