from numpy import random

from data_generation.neighborhood_generation.gpdatagenerator import calculate_feature_values
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.helper import *
from data_generation.global_data_generation import global_data_generator
from enums.diff_classifier_method_type import diff_classifier_method_type

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier

from datetime import datetime
from sklearn.cluster import KMeans
from random import randrange, uniform

import _pickle as cPickle

def create_synth_data(bottom_left,top_right,ninstances, blackbox1, blackbox2):
    location = list()
    value = np.zeros(ninstances,float)
    i = 0
    while i < ninstances:

        current = [random.uniform(bottom_left[0], top_right[0]),random.uniform(bottom_left[1], top_right[1])]
        location.append(current)
        i+=1

    X1 = []
    y1 = []
    i = 0
    for x_0, y_0 in zip(location, value):
        if i % 2 == 0:
            if blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0:
                y_0 = 0
            else:
                y_0 = 1
        else:
            if blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0:
                y_0 = 0
            else:
                y_0 = 1

        X1.append(x_0)
        y1.append(y_0)
        i += 1

    X1 = np.asarray(X1)
    y1 = np.asarray(y1)

    return X1, y1

