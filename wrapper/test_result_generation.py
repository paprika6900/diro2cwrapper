import matplotlib.pyplot as plt
from mlxtend.plotting import plot_decision_regions
from sklearn.tree import DecisionTreeClassifier

import diro2c
from data_generation.helper import *
from data_generation.neighborhood_generation import neighbor_generator
from enums.diff_classifier_method_type import diff_classifier_method_type
from location_generation import location_generation
from synth_data_generation import synth_data_generation
from wrapper.clusteringType import ClusteringType


def run_test(clusteringType, nclusters,X1,y1,dataset, blackbox1, blackbox2,bottomLeft,topRight,gax):
    print(clusteringType)
    xlim = [bottomLeft[0], topRight[0]]
    ylim = [bottomLeft[1], topRight[1]]

    # run wrapper on testdata in order to generate new instances and list of chosen instances
    if clusteringType == ClusteringType.GRID :
        extended_data, explained_instances = location_generation.generate_grid_locations(X1,xlim,ylim,nclusters)
    if clusteringType == ClusteringType.MAXLINKAGE or clusteringType == ClusteringType.WARDS:
        extended_data, explained_instances = location_generation.generate_agglomerative_locations(clusteringType,blackbox1, blackbox2, X1, y1,nclusters)
    if clusteringType == ClusteringType.RANDOM :
        extended_data, explained_instances = location_generation.generate_random_locations(X1,nclusters)

    # apply diro2c to chosen locations
    explained_locations, X_diff, y_diff = location_generation.apply_global_diro2c(explained_instances, extended_data, dataset, blackbox1, blackbox2)
    print(explained_locations[:])


    dc_full = DecisionTreeClassifier(random_state=0)
    dc_full.fit(X_diff, y_diff)

    # create plot with subplots and do plotting for different steps
    fig, ax = plt.subplots(1, 4, figsize=(16, 8))
    location_generation.plot_blackbox(blackbox1, X1, y1, ax[0],xlim,ylim)
    location_generation.plot_blackbox(blackbox2, X1, y1, ax[1],xlim,ylim)
    location_generation.plot_confusion_matrix(blackbox1, blackbox2, X1, y1, ax[2],xlim,ylim)
    location_generation.plot_chosenLocations(ax[2], explained_locations, clusteringType =clusteringType)

    location_generation.plot_diff_classifier(ax[3], X_diff, y_diff, dc_full,xlim,ylim)
    location_generation.plot_chosenLocations(ax[3], explained_locations, clusteringType =clusteringType)
    ax[3].set_title(clusteringType.name + " Diff Classifier")

    location_generation.plot_diff_classifier(gax, X_diff, y_diff, dc_full, xlim, ylim)
    location_generation.plot_chosenLocations(gax, explained_locations, clusteringType =clusteringType)
    ax[0].set_title("A) " + ax[0].get_title())
    ax[1].set_title("B) " + ax[1].get_title())
    ax[2].set_title("C) " + ax[2].get_title())
    ax[3].set_title("D) " + ax[3].get_title())

    gax.set_title(clusteringType.name)

    print('Number of Generated Points =', X_diff.__len__())

    accuracy, f1_score, mcc = location_generation.calc_diff_scores(X1, y1, blackbox1, blackbox2, dc_full)
    print('accuracy = ',accuracy)
    print('f1 Score = ', f1_score)
    print('mcc = ', mcc)

    fig.savefig(blackbox2.name + '_' + clusteringType.name + '.png', bbox_inches='tight')
    return

def run_test_statistiks(clusteringType, nclusters,X1,y1,dataset, blackbox1, blackbox2,bottomLeft,topRight):
    xlim = [bottomLeft[0], topRight[0]]
    ylim = [bottomLeft[1], topRight[1]]

    # run wrapper on testdata in order to generate new instances and list of chosen instances
    if clusteringType == ClusteringType.GRID :
        extended_data, explained_instances = location_generation.generate_grid_locations(X1,xlim,ylim,nclusters)
    if clusteringType == ClusteringType.MAXLINKAGE or clusteringType == ClusteringType.WARDS:
        extended_data, explained_instances = location_generation.generate_agglomerative_locations(clusteringType,blackbox1, blackbox2, X1, y1,nclusters)
    if clusteringType == ClusteringType.RANDOM :
        extended_data, explained_instances = location_generation.generate_random_locations(X1,nclusters)

    # apply diro2c to chosen locations
    explained_locations, X_diff, y_diff = location_generation.apply_global_diro2c(explained_instances, extended_data, dataset, blackbox1, blackbox2)

    dc_full = DecisionTreeClassifier(random_state=0)
    dc_full.fit(X_diff, y_diff)

    accuracy, f1_score, mcc = location_generation.calc_diff_scores(X1, y1, blackbox1, blackbox2, dc_full)
    print(mcc,',', end = '')


    return

