import matplotlib.pyplot as gplt
from mlxtend.plotting import plot_decision_regions
from sklearn.tree import DecisionTreeClassifier

import diro2c
from blackbox import blackbox_lt_0

from data_generation.helper import *
from data_generation.neighborhood_generation import neighbor_generator
from enums.diff_classifier_method_type import diff_classifier_method_type
from location_generation import location_generation
from synth_data_generation import synth_data_generation
from wrapper.blackbox import blackbox_falling, blackbox_sinus_falling, blackbox_isolated, blackbox_sinus_broad, \
    blackbox_sinus_reducing
from wrapper.clusteringType import ClusteringType
from wrapper.diff_blackbox.diff_blackbox import DiffBlackbox
from wrapper.printGroundTruths import printGroundTruth
from wrapper.test_result_generation import run_test, run_test_statistiks
import sys

def main(bb1, bb2, nagglom = (2,2,2,2), nrand = 8, ngrid = (2,4) ):

    bottomLeft = (0,-40)
    topRight = (50,40)

    # define blackboxes to use
    blackbox1 = bb1
    blackbox2 = bb2


    #generate synth 2d data that could have been used to train blackboxes
    X1, y1 = synth_data_generation.create_synth_data(bottomLeft,topRight,4000, blackbox1 , blackbox2)

    #split locations into axes (feature1 , feature2)
    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    feature1 = np.asarray(feature1)
    feature2 = np.asarray(feature2)

    y = y1
    y = y.astype(str)

    d = {'y': y, 'feature_1': feature1, 'feature_2': feature2}
    df = pd.DataFrame(d)
    dataset = prepare_df(df, 'test', 'y')

    nclusters = nagglom
    run_test_statistiks(ClusteringType.WARDS,nclusters,X1,y1,dataset,blackbox1,blackbox2,bottomLeft,topRight)
    run_test_statistiks(ClusteringType.MAXLINKAGE,nclusters, X1, y1, dataset, blackbox1, blackbox2,bottomLeft,topRight)
    nclusters = nrand
    run_test_statistiks(ClusteringType.RANDOM, nclusters, X1, y1, dataset, blackbox1, blackbox2,bottomLeft,topRight)
    nclusters = ngrid
    run_test_statistiks(ClusteringType.GRID, nclusters, X1, y1, dataset, blackbox1, blackbox2,bottomLeft,topRight)
    print("");

def run(bb1,bb2):

    print("80")
    main(bb1, bb2, nagglom=(20, 20, 20, 20), nrand=(80), ngrid=(10, 8))
    main(bb1, bb2, nagglom=(20, 20, 20, 20), nrand=(80), ngrid=(10, 8))
    main(bb1, bb2, nagglom=(20, 20, 20, 20), nrand=(80), ngrid=(10, 8))
    main(bb1, bb2, nagglom=(20, 20, 20, 20), nrand=(80), ngrid=(10, 8))
    main(bb1, bb2, nagglom=(20, 20, 20, 20), nrand=(80), ngrid=(10, 8))

if __name__ == "__main__":
    sys.stdout = open('results.txt', 'w')
    print('WARDS, MAXLINKAGE, RANDOM, GRID')
    run(blackbox_lt_0,blackbox_sinus_broad)


    #main(blackbox_lt_0,blackbox_isolated,nagglom = (2,2,2,2), nrand = 8, ngrid = (2,4))
    #main(blackbox_lt_0,blackbox_sinus_broad,nagglom = (2,2,2,2), nrand = 8, ngrid = (2,4))
    #main(blackbox_lt_0, blackbox_sinus_reducing, nagglom=(6, 6, 6, 6), nrand=24, ngrid=(6,4))
    #main(blackbox_falling, blackbox_sinus_falling, nagglom=(4, 4, 4, 4), nrand=16, ngrid=(4, 4))
