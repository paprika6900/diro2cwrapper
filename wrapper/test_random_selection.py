import matplotlib.pyplot as plt
from mlxtend.plotting import plot_decision_regions
from sklearn.tree import DecisionTreeClassifier

import diro2c
from blackbox import blackbox_lt_0
from blackbox import blackbox_sinus
from data_generation.helper import *
from data_generation.neighborhood_generation import neighbor_generator
from enums.diff_classifier_method_type import diff_classifier_method_type
from location_generation import location_generation
from synth_data_generation import synth_data_generation


def main():
    # define blackboxes to use
    blackbox1 = blackbox_lt_0
    blackbox2 = blackbox_sinus

    #generate synth 2d data that could have been used to train blackboxes
    X1, y1 = synth_data_generation.create_synth_data((0,-30),(40,30),500, blackbox1 , blackbox2)

    #split locations into axes (feature1 , feature2)
    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    feature1 = np.asarray(feature1)
    feature2 = np.asarray(feature2)

    y = y1
    y = y.astype(str)

    #prepare pandas dataframe from testdata
    d = {'y': y, 'feature_1': feature1, 'feature_2': feature2}
    df = pd.DataFrame(d)
    dataset = prepare_df(df, 'test', 'y')


    #define number of clusters for clustering
    locations = 5
    #define scope
    xlim = [0, 40]
    ylim = [-30, 30]
    #run wrapper on testdata in order to generate new instances and list of chosen instances
    extended_data, explained_instances = location_generation.generate_random_locations(X1,locations)
    #apply diro2c to chosen locations
    explained_locations, X_diff,y_diff = location_generation.apply_global_diro2c(explained_instances,extended_data,dataset,blackbox1,blackbox2)

    dc_full = DecisionTreeClassifier(random_state=0)
    dc_full.fit(X_diff, y_diff)

    #create plot with subplots and do plotting for different steps
    fig, ax = plt.subplots(1, 3, figsize=(16, 8))
    location_generation.plot_blackbox(blackbox1,X1,y1,ax[0])
    location_generation.plot_blackbox(blackbox2,X1,y1,ax[1])

    location_generation.plot_diff_classifier(ax[2],X_diff,y_diff,dc_full)
    location_generation.plot_chosenLocations(ax[2],explained_locations)

    print(location_generation.calc_f1_score_diff(X1, y1, blackbox1, blackbox2, dc_full))

    plt.show()


if __name__ == "__main__":
    main()
