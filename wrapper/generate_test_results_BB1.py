import matplotlib.pyplot as gplt
from mlxtend.plotting import plot_decision_regions
from sklearn.tree import DecisionTreeClassifier

import diro2c
from blackbox import blackbox_lt_0

from data_generation.helper import *
from data_generation.neighborhood_generation import neighbor_generator
from enums.diff_classifier_method_type import diff_classifier_method_type
from location_generation import location_generation
from synth_data_generation import synth_data_generation
from wrapper.blackbox import blackbox_falling, blackbox_sinus_falling, blackbox_isolated, blackbox_sinus_broad, \
    blackbox_sinus_reducing
from wrapper.clusteringType import ClusteringType
from wrapper.diff_blackbox.diff_blackbox import DiffBlackbox
from wrapper.printGroundTruths import printGroundTruth
from wrapper.test_result_generation import run_test
import sys

def main(bb1, bb2, nagglom = (2,2,2,2), nrand = 8, ngrid = (2,4) ):

    bottomLeft = (0,-40)
    topRight = (50,40)

    # define blackboxes to use
    blackbox1 = bb1
    blackbox2 = bb2


    #blackbox1 = blackbox_falling
    #blackbox2 = blackbox_sinus_falling
    sys.stdout = open(blackbox2.name + '_results.txt', 'w')
    print(blackbox1.name)
    print(blackbox2.name)



    #generate synth 2d data that could have been used to train blackboxes
    X1, y1 = synth_data_generation.create_synth_data(bottomLeft,topRight,4000, blackbox1 , blackbox2)

    diff_black_box = DiffBlackbox(blackbox1,blackbox2);
    printGroundTruth(diff_black_box,X1,y1)

    #split locations into axes (feature1 , feature2)
    feature1 = []
    feature2 = []
    for x in X1:
        feature1.append(x[0])
        feature2.append(x[1])

    feature1 = np.asarray(feature1)
    feature2 = np.asarray(feature2)

    y = y1
    y = y.astype(str)

    #prepare pandas dataframe from testdata
    d = {'y': y, 'feature_1': feature1, 'feature_2': feature2}
    df = pd.DataFrame(d)
    dataset = prepare_df(df, 'test', 'y')
    # define number of clusters for clustering
    gfig, gax = gplt.subplots(1, 4, figsize=(16, 8))
    #Balance cluster amount
    #More Functions
    nclusters = nagglom
    run_test(ClusteringType.WARDS,nclusters,X1,y1,dataset,blackbox1,blackbox2,bottomLeft,topRight,gax[2]);
    run_test(ClusteringType.MAXLINKAGE,nclusters, X1, y1, dataset, blackbox1, blackbox2,bottomLeft,topRight,gax[3]);
    nclusters = nrand
    run_test(ClusteringType.RANDOM, nclusters, X1, y1, dataset, blackbox1, blackbox2,bottomLeft,topRight,gax[0]);
    nclusters = ngrid
    run_test(ClusteringType.GRID, nclusters, X1, y1, dataset, blackbox1, blackbox2,bottomLeft,topRight,gax[1]);
    gax[0].set_title("A) "+ gax[0].get_title())
    gax[1].set_title("B) " + gax[1].get_title())
    gax[2].set_title("C) " + gax[2].get_title())
    gax[3].set_title("D) " + gax[3].get_title())
    gfig.savefig(blackbox2.name + '_allAlg.png', bbox_inches='tight')

if __name__ == "__main__":
    #main(blackbox_lt_0,blackbox_isolated,nagglom = (2,2,2,2), nrand = 8, ngrid = (2,4))
    #main(blackbox_lt_0,blackbox_sinus_broad,nagglom = (2,2,2,2), nrand = 8, ngrid = (2,4))
    #main(blackbox_lt_0, blackbox_sinus_reducing, nagglom=(6, 6, 6, 6), nrand=24, ngrid=(6,4))
    main(blackbox_falling, blackbox_sinus_falling, nagglom=(4, 4, 4, 4), nrand=16, ngrid=(4, 4))
