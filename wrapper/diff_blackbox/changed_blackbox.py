import numpy as np

from wrapper.blackbox import blackbox_falling, blackbox_sinus_falling

name = ""
blackbox1 = None
blackbox2 = None


class ChangedBlackBox:
    def __init__(self, blackbox1):
        self.blackbox1 = blackbox1


    def predict(self,data):
        blackbox1 = self.blackbox1

        x = list()
        for i in data:
            value = 0
            if i[8] <= 1500 or i[8] >= 3500:
                if (blackbox1.predict(i.reshape(1, -1))==0):
                    value = 1
                else:
                    value = 0
            else:
                value = blackbox1.predict(i.reshape(1, -1))
            x.append(value)

        return np.array(x)

