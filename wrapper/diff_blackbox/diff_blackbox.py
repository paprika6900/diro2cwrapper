import numpy as np

from wrapper.blackbox import blackbox_falling, blackbox_sinus_falling

name = ""
blackbox1 = None
blackbox2 = None


class DiffBlackbox:
    def __init__(self, blackbox1, blackbox2):
        self.blackbox1 = blackbox1
        self.blackbox2 = blackbox2
        self.name = blackbox2.name


    def predict(self,data):
        blackbox1 = self.blackbox1
        blackbox2 = self.blackbox2
        x = []
        for i in data:
            value = 0
            if (blackbox1.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 0) and (blackbox2.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 0):
                value = 0
            if (blackbox1.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 1) and (blackbox2.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 0):
                value = 2
            if (blackbox1.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 0) and (blackbox2.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 1):
                value = 3
            if (blackbox1.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 1) and (blackbox2.predict(np.array((i[0],i[1])).reshape(1, -1))[0] == 1):
                value = 1
            x.append(value)

        return np.array(x)

