from random import randrange

import matplotlib.pyplot as plt
import sklearn.metrics
from mlxtend.plotting import plot_decision_regions
from sklearn.metrics import f1_score, matthews_corrcoef, accuracy_score

import diro2c
from data_generation.neighborhood_generation.gpdatagenerator import calculate_feature_values
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.helper import *
from data_generation.global_data_generation import global_data_generator
from enums.diff_classifier_method_type import diff_classifier_method_type

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import NearestCentroid

from datetime import datetime
from sklearn.cluster import KMeans

import _pickle as cPickle

from wrapper import clusteringType
from wrapper.clusteringType import ClusteringType


def create_confusion_matrix(blackbox1, blackbox2, X1, y1):
    confusion_matrix = list()
    confusion_matrix.append(list())
    confusion_matrix.append(list())
    confusion_matrix.append(list())
    confusion_matrix.append(list())
    i = 0
    a = 0
    b = 0
    c = 0
    d = 0
    while i < len(X1):
        x_0 = X1[i]
        y_0 = y1[i]
        if (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0 and
                blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0):
            confusion_matrix[0].append(i)
            #if a<=10 :
                #print("00:")
                #print(x_0)
                #print(y_0)
                #a+=1
        elif (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1 and
              blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1):
            confusion_matrix[1].append(i)
            #if b<=10 :
                #print("11:")
                #print(x_0)
                #print(y_0)
                #b += 1
        elif (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1 and
              blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0):
            confusion_matrix[2].append(i)
            #if c<=10 :
                #print("10:")
                #print(x_0)
                #print(y_0)
                #c += 1
        elif (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0 and
              blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1):
            confusion_matrix[3].append(i)
            #if d<=10 :
                #print("01:")
                #print(x_0)
                #print(y_0)
                #d += 1
        i += 1

    #print(len(confusion_matrix[0]))
    #print(len(confusion_matrix[1]))
    #print(len(confusion_matrix[2]))
    #print(len(confusion_matrix[3]))


    return confusion_matrix


def k_means_centers(point_list, nclusters=1, ninit=10, maxiter=300):
    if len(point_list) != 0:
        if len(point_list) < nclusters:
            nclusters = len(point_list)
        km_centers = list()
        km = KMeans(
            n_clusters=nclusters,
            n_init=ninit, max_iter=maxiter,
        )
        y_km = km.fit_predict(point_list[:])
        for cluster_center in km.cluster_centers_:
            km_centers.append(cluster_center)

        return km_centers
    else:
        raise ValueError('cannot compute k_means for empty list')

def find_centers(clusteringType,point_list, nclusters=1, ninit=10, maxiter=300):
    if len(point_list) != 0:
        centers = list()
        if len(point_list) < nclusters:
            if len(point_list) ==1:
                return point_list
            nclusters = len(point_list)
        if nclusters == 1:
            length = point_list.shape[0]
            sum_x = np.sum(point_list[:, 0])
            sum_y = np.sum(point_list[:, 1])
            cluster_center =  sum_x / length, sum_y / length
            centers.append(cluster_center)
        else:
            if clusteringType == ClusteringType.WARDS :
                agclust = AgglomerativeClustering(affinity='euclidean', compute_full_tree='auto',
                                              connectivity=None, linkage='ward', memory=None,
                                              n_clusters=nclusters).fit_predict(point_list[:])
            else :
                agclust = AgglomerativeClustering(affinity='euclidean', compute_full_tree='auto',
                                              connectivity=None, linkage='complete', memory=None,
                                              n_clusters=nclusters).fit_predict(point_list[:])
            clf = NearestCentroid()
            clf.fit(point_list[:], agclust)
            for cluster_center in clf.centroids_:
                centers.append(cluster_center)

        return centers
    else:
        raise ValueError('cannot compute k_means for empty list')


# returns an extended Dataset containing generated Centers for the Confusion Matrix
# returns a list of indexes where generated instances are added
def generate_agglomerative_locations(clusteringType,blackbox1, blackbox2, X1, y1, nclusters):
    km_centers = list()
    explained_instances = list()
    if clusteringType != ClusteringType.WARDS and clusteringType != ClusteringType.MAXLINKAGE:
        raise ValueError('not agglomerative clustering type provided')

    confusion_matrix = create_confusion_matrix(blackbox1, blackbox2, X1, y1)

    if len(confusion_matrix[0]) != 0:
        km_centers.extend(find_centers(clusteringType,X1[confusion_matrix[0][:]], nclusters=nclusters[0]))
    if len(confusion_matrix[1]) != 0:
        km_centers.extend(find_centers(clusteringType,X1[confusion_matrix[1][:]], nclusters=nclusters[1]))
    if len(confusion_matrix[2]) != 0:
        km_centers.extend(find_centers(clusteringType,X1[confusion_matrix[2][:]], nclusters=nclusters[2]))
    if len(confusion_matrix[3]) != 0:
        km_centers.extend(find_centers(clusteringType,X1[confusion_matrix[3][:]], nclusters=nclusters[3]))

    #extended_data = np.append(X1, km_centers, 0)
    extended_data = np.asarray(km_centers)

    i = len(extended_data) - 1
    for point in km_centers:
        explained_instances.append(i)
        i -= 1

    return extended_data, explained_instances

# returns an extended Dataset containing generated Centers
# returns a list of indexes where generated instances are added
def generate_unsorted_locations(X1, nclusters):
    km_centers = list()
    explained_instances = list()

    km_centers.extend(wards_centers(X1[:], nclusters=nclusters))

    extended_data = np.append(X1, km_centers, 0)

    i = len(extended_data) - 1
    for point in km_centers:
        explained_instances.append(i)
        i -= 1

    return extended_data, explained_instances


def generate_grid_locations(X1, xlim, ylim, nclusters):
    x = (xlim[0] * (-1)) + xlim[1]
    y = (ylim[0] * (-1)) + ylim[1]

    x_dist = x / (nclusters[0]-1)
    y_dist = y / (nclusters[1]-1)

    grid_centers = list()
    explained_instances = list()

    x = xlim[0]
    i = 0
    while i < nclusters[0]:
        y = ylim[0]
        j = 0
        while j < nclusters[1]:
            x = round(x,2)
            y = round(y,2)

            if x==0:
                x+=0.1
            if y==0:
                y+=0.1
            grid_centers.append([x, y])
            y += y_dist
            j += 1
        x += x_dist
        i += 1

    #extended_data = np.append(X1, grid_centers, 0)
    extended_data = np.asarray(grid_centers)

    i = len(extended_data) - 1
    for point in grid_centers:
        explained_instances.append(i)
        i -= 1

    return extended_data, explained_instances


def generate_random_locations(X1, nclusters):
    explained_instances = list()
    locations = []
    i = 0
    while i < nclusters:
        value = randrange(0, (len(X1) - 1))
        explained_instances.append(i)
        locations.append(X1[value])
        i += 1

    locations = np.asarray(locations)
    return locations, explained_instances


def apply_global_diro2c(explained_instances, extended_data, dataset, blackbox1, blackbox2,gn_population_size=1000,classifier_type = diff_classifier_method_type.multiclass_diff_classifier):
    explained_locations = list()
    X_diff = None
    y_diff = None

    i = 0
    while i < len(explained_instances):

        diff_classifiers_info = diro2c.recognize_diff((explained_instances[i]), extended_data, dataset, blackbox1,
                                                      blackbox2,
                                                      classifier_type,
                                                      data_generation_function=neighbor_generator.get_modified_genetic_neighborhood,
                                                      continuous_function_estimation=False, gn_population_size=gn_population_size)
        if classifier_type == diff_classifier_method_type.multiclass_diff_classifier:
            dc_info = diff_classifiers_info['multiclass_diff_classifer']
        elif classifier_type == diff_classifier_method_type.binary_diff_classifier:
            dc_info = diff_classifiers_info['binary_diff_classifer']
        else:
            dc_info = None

        evaluation_info = dc_info['evaluation_info']

        if X_diff is None and y_diff is None:
            X_diff = evaluation_info['X']
            y_diff = evaluation_info['y']
        else:
            X_diff = np.concatenate((X_diff, evaluation_info['X']))
            y_diff = np.concatenate((y_diff, evaluation_info['y']))

        explained_locations.append(extended_data[explained_instances[i]])
        i += 1

    return explained_locations, X_diff, y_diff


def plot_confusion_matrix(blackbox1, blackbox2, X1, y1, ax,xlim=None,ylim=None):
    confusion_matrix = create_confusion_matrix(blackbox1, blackbox2, X1, y1)

    markers = "s^oxv<>"
    colors = (
        "#1f77b4", "#ff7f0e", "#3ca02c", "#d62728"
    )

    if xlim != None:
        ax.set_xlim(xlim)
    if ylim != None:
        ax.set_ylim(ylim)
    ax.set_title('Confusion Matrix')
    j = 0
    while j < 4:
        i = 0
        while i < len(confusion_matrix[j]):
            x = X1[confusion_matrix[j][i]][0]
            y = X1[confusion_matrix[j][i]][1]
            ax.scatter(
                x,
                y,
                c=colors[j],
                marker=markers[j],
                edgecolors=None,
                alpha=0.7
            )
            i += 1
        j += 1

def plot_points(X1, y1, ax):
    markers = "s^oxv<>"
    colors = (
        "#1f77b4", "#ff7f0e", "#3ca02c", "#d62728"
    )
    ax.set_title('Confusion Matrix')
    for point,value in zip(X1,y1):
        x = point[8]
        y = point[10]
        ax.scatter(
            x,
            y,
            c=colors[value],
            marker=markers[value],
            edgecolors="black",
            alpha=1.0
        )


def plot_blackbox(blackbox, X1, y1, ax,xlim=None,ylim=None):
    if hasattr(blackbox,'func'):
        ax.set_title(blackbox.func)
    scatter_kwargs = {'edgecolor': None, 'alpha': 0.7}
    plot_decision_regions(X=X1, y=y1, clf=blackbox, ax=ax, legend=2,
                          hide_spines=False, scatter_kwargs = scatter_kwargs)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles,
            ['A) 00', 'B) 11'],
             framealpha=0.7, scatterpoints=1, loc = 'upper left')

    if xlim != None:
        ax.set_xlim(xlim)
    if ylim != None:
        ax.set_ylim(ylim)


def plot_chosenLocations(ax, explained_locations,index_x=0,index_y=1,clusteringType = ClusteringType.GRID):
    i = 0
    length = len(explained_locations)
    while i < length:
        if(clusteringType == ClusteringType.WARDS or clusteringType == ClusteringType.MAXLINKAGE):
            if(i<4):
                markeredgecolor = "#d62728"
                markerfacecolor = "black"
            elif (i < 8):
                markeredgecolor = "#3ca02c"
                markerfacecolor = "black"
            elif (i < 12):
                markeredgecolor = "#ff7f0e"
                markerfacecolor = "black"
            elif (i < 16):
                markeredgecolor = "#1f77b4"
                markerfacecolor = "black"
        else:
            markeredgecolor = "black"
            markerfacecolor = "yellow"
        ax.plot(explained_locations[i][index_x], explained_locations[i][index_y], 'o', markersize=5, markeredgecolor=markerfacecolor,
                markerfacecolor=markeredgecolor, markeredgewidth=2)
        i += 1


def plot_diff_classifier(ax, X_diff, y_diff, dc_full,xlim=None,ylim=None):
    ax.set_title('Multi class diff-classifier')



    scatter_kwargs = {'edgecolor': None, 'alpha':0.5}
    plot_decision_regions(X=X_diff, y=y_diff, clf=dc_full, legend=2,
                          hide_spines=False,ax=ax, scatter_kwargs = scatter_kwargs)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles,
            ['A) 00', 'B) 11', 'C) 10', 'D) 01'],
             framealpha=0.7, scatterpoints=1, loc = 'upper left')

    if xlim != None:
        ax.set_xlim(xlim)
    if ylim != None:
        ax.set_ylim(ylim)






def calc_f1_score(X1, y1, blackbox):
    y_true = y1
    y_pred = list()

    i = 0
    while i < len(X1):
        x_0 = X1[i]
        y_0 = y1[i]
        y_pred.append(blackbox.predict(np.array(x_0, y_0).reshape(1, -1))[0])
        i += 1

    return f1_score(y_true, y_pred, average='macro')

def calc_diff_scores(X1,y1,blackbox1,blackbox2,diff_cl):
    y_true = list()
    y_pred = list()
    i = 0
    while i < len(X1):
        x_0 = X1[i]
        y_0 = y1[i]
        pred = diff_cl.predict(np.array(x_0, y_0).reshape(1, -1))[0]
        if pred <= 1:
            y_pred.append(0)
        if pred >= 2:
            y_pred.append(1)

        if (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0 and
                blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0):
            y_true.append(0)
        elif (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1 and
              blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1):
            y_true.append(0)
        elif (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1 and
              blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0):
            y_true.append(1)
        elif (blackbox1.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 0 and
              blackbox2.predict(np.array(x_0, y_0).reshape(1, -1))[0] == 1):
            y_true.append(1)
        i+=1

    return accuracy_score(y_true,y_pred), f1_score(y_true, y_pred, average='binary'), matthews_corrcoef(y_true,y_pred)