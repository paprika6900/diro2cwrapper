from random import randrange

import matplotlib.pyplot as plt
import sklearn.metrics
from mlxtend.plotting import plot_decision_regions
from sklearn.metrics import f1_score, matthews_corrcoef, accuracy_score

import diro2c
from data_generation.neighborhood_generation.gpdatagenerator import calculate_feature_values
from data_generation.neighborhood_generation import neighbor_generator
from data_generation.helper import *
from data_generation.global_data_generation import global_data_generator
from enums.diff_classifier_method_type import diff_classifier_method_type

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import NearestCentroid

from datetime import datetime
from sklearn.cluster import KMeans

import _pickle as cPickle

from wrapper.clusteringType import ClusteringType

label = ["age", "workclass", "education", "marital-status", "occupation", "relationship", "race", "sex",
         "capital-gain", "capital-loss", "hours-per-week", "nativecountry"]
constraints = [0, (0, 7), (0, 15), (0, 6), (0, 13), (0, 5), (0, 4), (0, 1), 0, 0, 0, (0, 36)]

def printLabels():
    print(label)
    return

def printAdultData(data):
    for row in data:
        print(row)
    return

def processAdultData(data):
    printLabels()
    printAdultData(data)
    adjusted_data = rounded_data = list()
    for row in data:
        adjusted_entry = list()
        i = 0
        while i < 12:
            value = round(row[i])
            if(constraints[i]== 0):
                adjusted_entry.append(value)
            else:
                if value < constraints[i][0]:
                    adjusted_entry.append(constraints[i][0])
                elif value > constraints[i][1]:
                    adjusted_entry.append(constraints[i][1])
                else:
                    adjusted_entry.append(value)
            i += 1
        rounded_data.append(adjusted_entry)
    printLabels()
    printAdultData(rounded_data)
    return rounded_data
