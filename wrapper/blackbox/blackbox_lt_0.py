import numpy as np

func = "y = 0"
name = "linear zero boundary"
def predict(data):
    x = []
    for i in data:
        if (i[1] > 0):
            x.append(0)
        else:
            x.append(1)
    
    return np.array(x)

