import numpy as np
import math

func = "y = sin(x)"
name = "sinus zero boundary"
def predict(data):
    x = []
    for i in data:
        if (i[1] < (math.sin((i[0]/(50/(math.pi * 4))))*(30))):
            x.append(1)
        else:
            x.append(0)
    
    return np.array(x)

