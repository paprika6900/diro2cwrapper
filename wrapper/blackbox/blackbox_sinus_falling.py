import numpy as np
import math

func = "y = sin(x) - x"
name = "sinus negative slope boundary"
def predict(data):
    x = []
    for i in data:
        if (i[1] < (math.sin(i[0]/2)*(20)) + ((-1)*i[0]/1.25+20)):
            x.append(1)
        else:
            x.append(0)
    
    return np.array(x)

