import numpy as np
import math

func = "y = sin(x)*(30-(x))"
name = "reducing sinus zero slope boundary"
def predict(data):
    x = []
    for i in data:
        if (i[1] < (math.sin(i[0]/2)*(30-(i[0]*(30/50))))):
            x.append(1)
        else:
            x.append(0)
    
    return np.array(x)

