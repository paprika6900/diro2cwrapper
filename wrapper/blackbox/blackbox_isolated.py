import numpy as np

func = "isolated areas"
name = "Isolated Difference Areas"
def predict(data):
    x = []
    for i in data:
        if (i[1] > 0):
            if(i[0]>40 and i[1]>30):
                x.append(1)
            else:
                x.append(0)
        else:
            if (i[0] < 10 and i[1] < -30):
                x.append(0)
            else:
                x.append(1)
    
    return np.array(x)

