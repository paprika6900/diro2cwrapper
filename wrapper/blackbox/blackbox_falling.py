import numpy as np

func = "y = -x"
name = "linear negative slope boundary"
def predict(data):
    x = []
    for i in data:
        if (i[1] >= ((-1)*i[0]/1.25+20)):
            x.append(0)
        else:
            x.append(1)
    
    return np.array(x)

