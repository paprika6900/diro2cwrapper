from enum import Enum


class ClusteringType(Enum):
    RANDOM = 1
    GRID = 2
    WARDS = 3
    MAXLINKAGE = 4