import matplotlib.pyplot as plt

from data_generation.helper import *
from synth_data_generation import synth_data_generation
from wrapper.blackbox import blackbox_falling, blackbox_sinus_falling, blackbox_sinus_broad, blackbox_sinus_reducing, \
    blackbox_isolated, blackbox_lt_0
from wrapper.diff_blackbox.diff_blackbox import DiffBlackbox
from wrapper.location_generation.location_generation import plot_blackbox
from wrapper.printGroundTruths import printGroundTruth


def main():
    bottomLeft = (0, -40)
    topRight = (50, 40)

    xlim = [bottomLeft[0], topRight[0]]
    ylim = [bottomLeft[1], topRight[1]]
    # define blackboxes to use
    blackbox1 = blackbox_lt_0
    blackbox5 = blackbox_falling
    blackbox3 = blackbox_sinus_broad
    blackbox4 = blackbox_sinus_reducing
    blackbox6 = blackbox_sinus_falling
    blackbox2 = blackbox_isolated

    X1 = [bottomLeft, topRight, (0.1, 0.1)]
    y1 = [0, 1, 1]

    X1 = np.asarray(X1)
    y1 = np.asarray(y1)

    fig, ax = plt.subplots(1, 6, figsize=(16, 8))
    plot_blackbox(blackbox1, X1, y1, ax[0], xlim, ylim);
    plot_blackbox(blackbox2, X1, y1, ax[1], xlim, ylim);
    plot_blackbox(blackbox3, X1, y1, ax[2], xlim, ylim);
    plot_blackbox(blackbox4, X1, y1, ax[3], xlim, ylim);
    plot_blackbox(blackbox5, X1, y1, ax[4], xlim, ylim);
    plot_blackbox(blackbox6, X1, y1, ax[5], xlim, ylim);
    ax[0].set_title("A) " + ax[0].get_title())
    ax[1].set_title("B) " + ax[1].get_title())
    ax[2].set_title("C) " + ax[2].get_title())
    ax[3].set_title("D) " + ax[3].get_title())
    ax[4].set_title("E) " + ax[4].get_title())
    ax[5].set_title("F) " + ax[5].get_title())

    plt.savefig("allClfs.png", bbox_inches='tight');

    fig, ax = plt.subplots(1, 4, figsize=(16, 8))

    X1, y1 = synth_data_generation.create_synth_data(bottomLeft, topRight, 1000, blackbox1, blackbox2)
    isolated = DiffBlackbox(blackbox1, blackbox2);
    printGroundTruth(isolated, X1, y1, ax[0])

    X1, y1 = synth_data_generation.create_synth_data(bottomLeft, topRight, 1000, blackbox1, blackbox3)
    broad = DiffBlackbox(blackbox1, blackbox3)
    printGroundTruth(broad, X1, y1, ax[1])

    X1, y1 = synth_data_generation.create_synth_data(bottomLeft, topRight, 1000, blackbox1, blackbox4)
    reducing = DiffBlackbox(blackbox1, blackbox4)
    printGroundTruth(reducing, X1, y1, ax[2])

    X1, y1 = synth_data_generation.create_synth_data(bottomLeft, topRight, 1000, blackbox5, blackbox6)
    falling = DiffBlackbox(blackbox5, blackbox6)
    printGroundTruth(falling, X1, y1, ax[3])

    ax[0].set_title("A) " + ax[0].get_title())
    ax[1].set_title("B) " + ax[1].get_title())
    ax[2].set_title("C) " + ax[2].get_title())
    ax[3].set_title("D) " + ax[3].get_title())

    plt.savefig("allGrndTruths.png", bbox_inches='tight');


if __name__ == "__main__":
    main()
